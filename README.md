# Bibata Sweet Cursor
###### Bibata Based Cursor Made for Sweet

![Bibata Sweet Example](https://gitlab.com/perseus.p.kane/bibata-sweet-cursors/raw/master/image/All_Cursor.png)

#Please note that this cursor set is still HEAVILY Work in Progress. This was copied over from [Bibata AdaptaBreath](https://gitlab.com/cscs/Bibata_AdaptaBreath_Cursors/tree/master). This still contains files from that project. They will be removed in due time.

# Installation
To install this cursor pack, simply run the following:

      $ git clone https://gitlab.com/perseus.p.kane/bibata-sweet-cursors.git
      $ cd bibata-sweet-cursors/
      $ chmod +x build.sh
      $ ./build.sh
      $ chmod +x ./Installer_Bibata.sh
      $ sudo ./Installer_Bibata.sh  

# Build
### Build Dependencies
##### Inkscape
##### xcursorgen

### How to Contribute to The Bibata Project
The source files can be found in the ['src'](https://gitlab.com/perseus.p.kane/bibata-sweet-cursors/tree/master/src) directory. The main cursor files are found in the ['svgs'](https://gitlab.com/perseus.p.kane/bibata-sweet-cursors/tree/master/src/Bibata_Sweet/svgs) folder inside of the ['Bibata_Sweet'](https://gitlab.com/perseus.p.kane/bibata-sweet-cursors/tree/master/src/Bibata_Sweet) folder. This folder contains all of the .svg files that make up the cursor pack. Feel free to edit them using Inkscape or any other SVG editor.

ENJOY.

Forked from original work by ['KaizIqbal'](https://github.com/KaizIqbal/Bibata_Cursor).
